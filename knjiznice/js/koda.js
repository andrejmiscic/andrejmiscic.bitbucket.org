
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

//podatki za generiranje
var dHeight = ["184", "185", "186", "188", "188"];
var dWeight = ["105", "102", "96", "93", "88"];
var dNasicenost = ["93", "96", "96", "96", "97"];
var dSis = ["140", "138", "130", "122", "117"];
var dDia = ["95", "94", "88", "83", "78"];
var dDates = ["2014-10-13T11:24Z", "2015-03-12T11:24Z", "2015-09-17T11:24Z", "2016-06-24T11:24Z", "2016-12-12T11:24Z"];

var cHeight = ["172", "172", "174", "175", "175"];
var cWeight = ["72", "73", "75", "76", "77"];
var cNasicenost = ["97", "98", "98", "98", "99"];
var cSis = ["118", "115", "112", "112", "110"];
var cDia = ["77", "77", "74", "71", "70"];
var cDates = ["2014-10-13T11:24Z", "2015-03-12T11:24Z", "2015-09-17T11:24Z", "2016-06-24T11:24Z", "2016-12-12T11:24Z"];

var aHeight = ["167", "168", "168", "168", "168"];
var aWeight = ["50", "52", "54", "55", "59"];
var aNasicenost = ["96", "97", "97", "97", "98"];
var aSis = ["117", "116", "115", "114", "112"];
var aDia = ["78", "77", "75", "74", "72"];
var aDates = ["2014-10-13T11:24Z", "2015-03-12T11:24Z", "2015-09-17T11:24Z", "2016-06-24T11:24Z", "2016-12-12T11:24Z"];

//ehr id cez celotno stran
var EHRID = "";
//bmi-ji
var bmis = [];

//za vreme
var slikce = {
    "clear sky": "01d.png",
    "few clouds": "02d.png",
    "scattered clouds": "03d.png",
    "broken clouds": "04d.png",
    "shower rain": "09d.png",
    "rain": "10d.png",
    "thunderstorm": "11d.png"
};

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 * (ime, priimek, spol, age, type)
 * (ehr, vis, teza, nas, sis, dia, date)
 */
function generirajPodatke() {
    //ustvari donalda
    ustvariTestnega("Donald", "Trump", "MALE", "70", 1);
    ustvariTestnega("Conor", "McGregor", "MALE", "28", 2);
    ustvariTestnega("Alenka", "Bratusek", "FEMALE", "47", 3);
}
    

// TODO: Tukaj countermplementirate funkcionalnost, ki jo podpira vaša aplikacija
function switchTabs(type){
    var tabContent = document.getElementsByClassName('tabcontent');
    for(var i = 0; i < tabContent.length; i++){
        tabContent[i].style.display = "none";
    }
    var tabs = document.getElementsByClassName('tab');
    for(i = 0; i < tabs.length; i++){
        tabs[i].className = tabs[i].className.replace(" active","");
    }
    if(type == 'novUporabnik'){
        document.getElementById("startTab").className += " active";
        document.getElementById('novUporabnik').style.display = "block";
    } else {
        document.getElementById("otherTab").className += " active";
        document.getElementById('obstojecUporabnik').style.display = "block";
    }
}

function switchMain(type){
    var tabContent = document.getElementsByClassName('maintabcontent');
    for(var i = 0; i < tabContent.length; i++){
        tabContent[i].style.display = "none";
    }
    var tabs = document.getElementsByClassName('maintab');
    for(i = 0; i < tabs.length; i++){
        tabs[i].className = tabs[i].className.replace(" active","");
    }
    if(type == 'vnos'){
        document.getElementById("dataTab").className += " active";
        document.getElementById('vnos').style.display = "block";
    } else if(type == 'forum') {
        document.getElementById("forumTab").className += " active";
        document.getElementById('forum').style.display = "block";
    } else if(type == 'diary'){
        document.getElementById("diaryTab").className += " active";
        document.getElementById('diary').style.display = "block";
    }
}

function ustvariTestnega(ime, priimek, spol, age, type){
    var sessionId = getSessionId();

    $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: "POST",
		    success: function(data){
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            gender: spol,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}, {key: "age", value: age}]
		        };
		         $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party){
		                if(party.action == 'CREATE'){//party.action vrne action, ki se je izvedu zarad requesta
		                    $("#odziv").html("<span class='label label-warning fade-in'>Uspesno!</span>");
		                    $("#vnesenEHR").val(ehrId);
		                    $("#ehrNavodila").html("Spodaj je izpisan vas EHR ID, prijavite se s klikom na gumb Prijava")
		                    switchTabs("obstojecUporabnik");
		                    if(type == 1){
		                        $("#odzivEHR").html("");
		                        $("#prvi").val(ehrId);
		                        for(var i = 0; i < 5; i++){
                                    vnosTestnih(ehrId, dHeight[i], dWeight[i], dNasicenost[i], dSis[i], dDia[i], dDates[i]);
                                }
                                $("#odzivEHR").append("<b> Donald Trump: </b>" + ehrId);
		                    } else if(type == 2){
		                        $("#drugi").val(ehrId);
		                        for(var i = 0; i < 5; i++){
                                    vnosTestnih(ehrId, cHeight[i], cWeight[i], cNasicenost[i], cSis[i], cDia[i], cDates[i]);
                                }
                                $("#odzivEHR").append("<b> Conor McGregor: </b>" + ehrId);
		                    } else {
		                        $("#tretji").val(ehrId);
		                        for(var i = 0; i < 5; i++){
                                    vnosTestnih(ehrId, aHeight[i], aWeight[i], aNasicenost[i], aSis[i], aDia[i], aDates[i]);
                                }
                                $("#odzivEHR").append("<b> Alenka Bratusek: </b>" + ehrId);
		                    }
		                }
		            },
		            error: function(error){
		                $("#odziv").html("<span class='label label-warning fade-in'>NAPAKA!</span>");
		            }
		         });
		    }
		});
}

function ustvariUporabnika(){
    var sessionId = getSessionId();
    
    var name = $("#ustvariIme").val();
    var surname = $("#ustvariPriimek").val();
    var age = $("#starost").val();
    var spol = $("#spol").val().trim();
    if(!name || !surname || !age || name.trim().length == 0 || surname.trim().length == 0 || age.trim().length == 0 || (spol != 'MALE' && spol != 'FEMALE')){
            $("#odziv").html("<span class='label label-warning fade-in'>Vnesite podatke!</span>");
    } else {
        $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: "POST",
		    success: function(data){
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: name,
		            lastNames: surname,
		            gender: spol,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}, {key: "age", value: age}]
		        };
		         $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party){
		                if(party.action == 'CREATE'){//party.action vrne action, ki se je izvedu zarad requesta
		                    $("#odziv").html("<span class='label label-warning fade-in'>Uspesno!</span>");
		                    $("#vnesenEHR").val(ehrId);
		                    $("#ehrNavodila").html("Spodaj je izpisan vas EHR ID, prijavite se s klikom na gumb Prijava")
		                    switchTabs("obstojecUporabnik");
		                }
		            },
		            error: function(error){
		                $("#odziv").html("<span class='label label-warning fade-in'>NAPAKA!</span>");
		            }
		         });
		    }
		})
    }
}

function vnosTestnih(ehr, vis, teza, nas, sis, dia, date){
    var sessionId = getSessionId();
    
    //console.log(ehr + " " + vis  + " " + teza  + " " + nas  + " " + date);
    //console.log(sessionId);
    $.ajaxSetup({
            headers: {"Ehr-Session": sessionId}
    });
    var podatki = {
        "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": date,
	    "vital_signs/height_length/any_event/body_height_length": vis,
	    "vital_signs/body_weight/any_event/body_weight": teza,
	    "vital_signs/blood_pressure/any_event/systolic": sis,
	    "vital_signs/blood_pressure/any_event/diastolic": dia,
	    "vital_signs/indirect_oximetry:0/spo2|numerator": nas
    };
    var parameters = {
        //action: "CREATE",
        ehrId: ehr,
	    templateId: "Vital Signs",
	    format: "FLAT"
    }
    $.ajax({
       url: baseUrl + "/composition?" + $.param(parameters), 
       type: 'POST',
       contentType: 'application/json',
       data: JSON.stringify(podatki),
       success: function (res) {},
       error: function(error){
           console.log(JSON.parse(error.responseText).userMessage);
       }
    });
}

function vnosPodatkov(){
    //TEST
    posodobljajAnalizo();
    
    var sessionId = getSessionId();
    
    var ehrId = $("#vnosEHR").val();
    var visina = $("#vnosVisina").val();
    var teza = $("#vnosTeze").val();
    var nasicenost = $("#vnosNasicenost").val();
    var sistolicni = $("#vnosSis").val();
    var diastolicni = $("#vnosDia").val();
    var datum = $("#vnosDatum").val();
    
    if(!ehrId || ehrId.trim().length == 0 || visina.trim().length == 0 || teza.trim().length == 0||datum.trim().length==0) {
        $("#odzivVnos").html("Vnesite vse podatke");
    } else {
        $.ajaxSetup({
            headers: {"Ehr-Session": sessionId}
        });
        var data = {
            "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datum,
		    "vital_signs/height_length/any_event/body_height_length": visina,
		    "vital_signs/body_weight/any_event/body_weight": teza,
		    "vital_signs/blood_pressure/any_event/systolic": sistolicni,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicni,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenost
        };
        var params = {
            action: "CREATE",
            ehrId: ehrId,
		    templateId: "Vital Signs",
		    format: "FLAT"
        }
        $.ajax({
           url: baseUrl + "/composition?" + $.param(params), 
           type: "POST",
           contentType: 'application/json',
           data: JSON.stringify(data),
           success: function (res) {}
        });
    }
}

function prijaviUporabnika(){
    EHRID = $("#vnesenEHR").val();
    $("#vnosEHR").val(EHRID);
    //TEST
    posodobljajAnalizo();
}

function posodobljajAnalizo(){
    var sessionId = getSessionId();
    
    if(EHRID == ""){
        $("#obvestilo").html("Prosimo, da se najprej prijavite ali s testnim ali z vašim Ehr IDjem!");
        $("#grafData").html("");
    } else {
        $("#obvestilo").html("");
        $("#grafData").html("Podatki o BMIjih:  ");
        
        $.ajax({
			url: baseUrl + "/demographics/ehr/" + EHRID + "/party",
			type: "GET",
			headers: {"Ehr-Session": sessionId},
			success: function (data) {
			    var name = data.party.firstNames + " " + data.party.lastNames;
			    $.ajax({
			        url: baseUrl + "/view/" + EHRID + "/height",
			        type: "GET",
			        headers: {"Ehr-Session": sessionId},
			        success: function(res_visina){
	                    $.ajax({
        			        url: baseUrl + "/view/" + EHRID + "/weight",
        			        type: "GET",
        			        headers: {"Ehr-Session": sessionId},
        			        success: function(res_teza){
			                    $.ajax({
                			        url: baseUrl + "/view/" + EHRID + "/blood_pressure",
                			        type: "GET",
                			        headers: {"Ehr-Session": sessionId},
                			        success: function(res_tlak){
        			                    $.ajax({
                        			        url: baseUrl + "/view/" + EHRID + "/spO2",
                        			        type: "GET",
                        			        headers: {"Ehr-Session": sessionId},
                        			        success: function(res_nasicenost){
                        			            $("#meritve").html("");
                        			            bmis = [];
                        			            var tabela = "<table class='table table-striped table-hover meritve'><tr><th class='text-center' colspan='2'>Podatki za: " + name + "</th></tr>";
                			                    for(var i = 0; i < res_teza.length; i++){
                			                        //tabela += "<tr class='res' id='master' onclick=showDetail(event)><td>" + res_teza[i].time + ":</td><td class='text-right'>" + res_visina[i].height + " cm,  " + res_teza[i].weight + " kg" + "</td></tr>";
                			                        tabela += "<tr class='res' id='master' onclick=showDetail(event)><td>Podatki za::</td><td class='text-right'>" + res_teza[i].time + "</td></tr>";
                			                        var bmi = res_teza[i].weight/(res_visina[i].height * res_visina[i].height/ 10000);
                			                        var bardata = {bmi: bmi, date: res_teza[i].time};
                			                        bmis.push(bardata);
                			                        //tabela += "<tr class='res details' id='detail'><td class='text-center'><p><b>BMI: " + bmi.toFixed(2) + "</b>  spO2: " + res_nasicenost[i].spO2 + "%</p></td><td class='text-center'><p>-sistolicni: " + res_tlak[i].systolic + " mmHg   -diastolicni: " + res_tlak[i].diastolic + " mmHg</p></td></tr>";
                			                        tabela += "<tr class='res details' id='detail'><td class='text-center'><p>spO2: " + res_nasicenost[i].spO2 + "%</p><p>-sistolicni: " + res_tlak[i].systolic + " mmHg   -diastolicni: " + res_tlak[i].diastolic + " mmHg</p></td>\
                			                                    <td class='text-center'><p>" + res_visina[i].height + " cm,  " + res_teza[i].weight + " kg" + "</p><p><b>BMI: " + bmi.toFixed(2) + "</b></td></tr>";
                			                    }
                			                    tabela += "</table>";
                			                    $("#meritve").append(tabela);
                			                    //user za chat
                			                    var pic = Math.floor((Math.random() * 90) + 1);
                			                    user = {id: pic, name: name};
                			                    //master detail
                                                $(".details").hide();
                                                //graf
                                                grafBMI();
                        			        }
                        			    });
                			        }
                			    });
        			        }
        			    });
    			     }
			    });
            }
        });
    }
}

function showDetail(event){
    var $target = $(event.target);
    var detail = $target.closest("tr").nextAll("tr[id]:first");
    detail.slideToggle();
}

function hideDetail(event){
    event.stopPropagation();
    var $target = $(event.target);
    $target.slideUp();
}

function grafBMI(){
    //spucam od prej
    $("#graph").html("");
    
    var margin = {top: 20, right: 20, bottom: 30, left: 20},
    width = 500 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;
    
    var x = d3.scaleBand().range([0, width]).padding(0.1);
    var y = d3.scaleLinear().range([height, 0]);
    
    var svg = d3.select("#graph").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        
    // formatiranje
    bmis.forEach(function(d){
        d.bmi = +d.bmi;
    });
    //meje
    x.domain(bmis.map(function(d){
        return d.date;
    }));
    y.domain([0, d3.max(bmis.map(function(d){
        return d.bmi;
    }))]);
    //posamezni pravokotnicki
    svg.selectAll(".bar")
        .data(bmis)
    .enter().append("rect")
    .attr("class", "bar")
    .attr("x", function(d) { return x(d.date); })
    .attr("width", x.bandwidth())
    .attr("y", function(d) { return y(d.bmi); })
    .attr("height", function(d) { return height - y(d.bmi); })
    .attr("fill", function(d){
        if(d.bmi > 30 || d.bmi < 18.6){
            return "crimson";
        } else if(d.bmi > 25 && d.bmi < 30){
            return "coral";
        } else{
            return "steelblue";
        }
    })
    .on("mouseover", function(d){
        $("#grafData").html("Podatki o BMIjih:  " + d.date + ":  <b>" + d.bmi.toFixed(2)) + "</b>";
    })
    .on("mouseout", function(d){
        $("#grafData").html("Podatki o BMIjih:  ");
    });
     
     //x:
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));
    
    //y os:
    svg.append("g")
        .call(d3.axisLeft(y));
}

function getLocation(){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(getWeather);
    }
}

function getWeather(position){
    $.ajax({
        url: "https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/2.5/weather?lat=" + position.coords.latitude.toFixed(0) + "&lon=" + position.coords.longitude.toFixed(0) + "&appid=b8220f66293677f34a7f19a2c5113a3a",
        type: "GET",
        success: function(data){
            var nasvet = "";
            if(data.weather[0].description == 'clear sky' || data.weather[0].description == 'few clouds'){
                nasvet = "SUPER! Danes lahko telovadis zunaj. Moznosti so neskončne: tek, kolesarjenje, ...";
            } else {
                nasvet = "SKODA! Danes je bolje, da vaje izvajaš v notranjih prostorih.";
            }
            $("#vreme").html("");
            $("#showWeather").append("\
            <div class='col-lg-12 col-md-12 col-sm-12'>\
                    <div class='col-lg-12 col-md-12 col-sm-12 panel_head panel-heading' align='center'>\
                        <h5><b>DANES</b></h5>\
                    </div>\
                <div class='panel-body mypanel>\
                    <table class='table table-striped'>\
                        <tr>\
                            <td><b>" + data.weather[0].main + "; " + data.weather[0].description + ".</b></td>\
                            <td><img src='http://openweathermap.org/img/w/" + slikce[data.weather[0].description] + "' /></td>\
                        </tr>\
                        <br>\
                        <tr>\
                            <td>Temperatura: " + (data.main.temp - 273).toFixed(1) + " stopinj C, vlaznost: " + data.main.humidity + ", veter: " + data.wind.speed + "km/h</td>\
                        </tr>\
                        <br>\
                        <tr>\
                            <td><b>" + nasvet + "</b></td>\
                        </tr>\
                    </table>\
                </div>\
            </div>");
        }
    });
    
    $.ajax({
        url: "https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/2.5/forecast?lat=" + position.coords.latitude.toFixed(0) + "&lon=" + position.coords.longitude.toFixed(0) + "&appid=b8220f66293677f34a7f19a2c5113a3a",
        type: "GET",
        success: function(data){
            for(var i = 1; i < 6; i++){
                var nasvet = "";
                if(data.list[i].weather[0].description == 'clear sky' || data.list[i].weather[0].description == 'few clouds'){
                    nasvet = "SUPER! Za ta dan načrtuj zunanjo aktivnost";
                } else {
                    nasvet = "SKODA! Za takrat planiraj notranjo aktivnost.";
                }
                $("#showWeather").append("\
                <div class='col-lg-12 col-md-12 col-sm-12'>\
                    <div class='col-lg-12 col-md-12 col-sm-12 panel_head panel-heading' align='center'>\
                        <h5><b>VREME ČEZ " + i + " DNI</b></h5>\
                    </div>\
                    <div class='col-lg-12 col-md-12 col-sm-12'>\
                        <tr>\
                            <td><b>" + data.list[i].weather[0].main + "; " + data.list[i].weather[0].description + ".</b><img src='http://openweathermap.org/img/w/" + slikce[data.list[i].weather[0].description] + "' /></td>\
                            <td align='right'>" + nasvet + "</td>\
                        </tr>\
                    </div>\
                </div>");
            }
        }
    });
}

//zacetek
$(document).ready(function(){
    //nastavim zacetni tab
    switchTabs('novUporabnik');
    switchMain('vnos');
    
    //uporabnik lahko izbere testnega uporabnika
    $("#testniUporabnik").change(function(){
        $("#vnesenEHR").val($(this).val());
    });
    //podatki in  graf
    posodobljajAnalizo();
    
    getLocation();
});